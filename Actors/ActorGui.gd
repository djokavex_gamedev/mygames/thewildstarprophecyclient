extends Node2D

onready var actor_name = get_node("Name")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	match get_parent().actor_type:
		PlayerStats.ActorType.PLAYER:
			get_node("MOOBar").hide()
			pass
		PlayerStats.ActorType.OTHER_PLAYER:
			get_node("DodgeBar").hide()
			get_node("MOOBar").hide()
			pass
		PlayerStats.ActorType.ENEMY:
			get_node("DodgeBar").hide()
			get_node("ManaBar").hide()
			pass
		_:
			print("ERROR: Unknown actor type !!!")
			pass
	pass # Replace with function body.

func InitializeHealth(max_health, current_health):
	get_node("HealthBar").max_value = max_health
	get_node("HealthBar").value = current_health
	if current_health == max_health:
		get_node("HealthBar").hide()
	else:
		get_node("HealthBar").show()
	pass

func ChangeHealth(max_health, current_health):
	get_node("HealthBar").max_value = max_health
	if current_health == max_health:
		get_node("HealthBar").hide()
	else:
		get_node("HealthBar").show()
	get_node("Tween").interpolate_property(get_node("HealthBar"), "value", get_node("HealthBar").value, current_health, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	pass

func InitializeMana(max_mana, current_mana):
	get_node("ManaBar").max_value = max_mana
	get_node("ManaBar").value = current_mana
	if current_mana == max_mana:
		get_node("ManaBar").hide()
	else:
		get_node("ManaBar").show()
	pass
	
func ChangeMana(max_mana, current_mana):
	get_node("ManaBar").max_value = max_mana
	if current_mana == max_mana:
		get_node("ManaBar").hide()
	else:
		get_node("ManaBar").show()
	get_node("Tween").interpolate_property(get_node("ManaBar"), "value", get_node("ManaBar").value, current_mana, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	pass

func ChangeDodge(current_dodge):
	if current_dodge == 100:
		get_node("DodgeBar").hide()
	else:
		get_node("DodgeBar").show()
	get_node("Tween").interpolate_property(get_node("DodgeBar"), "value", get_node("DodgeBar").value, current_dodge, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	pass

func ChangeMOO(max_moo, current_moo):
	get_node("MOOBar").max_value = max_moo
	if current_moo == max_moo:
		get_node("MOOBar").hide()
	else:
		get_node("MOOBar").show()
	get_node("Tween").interpolate_property(get_node("MOOBar"), "value", get_node("MOOBar").value, current_moo, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	pass
