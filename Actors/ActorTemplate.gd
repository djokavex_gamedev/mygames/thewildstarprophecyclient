extends KinematicBody2D

var FloatingTextClass = preload("res://Assets/Other/FloatingText.tscn")

var actor_type = PlayerStats.ActorType.PLAYER
var motion = Vector2(0,0)
var previous_position = Vector2.ZERO

var max_health = 500
var health = 500
var state = "Idle"



# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	match actor_type:
		PlayerStats.ActorType.PLAYER:
			set_physics_process(false)
			get_node("HitBox/HitBoxSprite").modulate = Color(0, 1.3, 0, 1)
			get_node("Camera2D").current = true
			get_node("ActorSprite").texture = load("res://Assets/img/player/16bit_" + str(PlayerStats.player_data["Sprite"]) + ".png")
			print(PlayerStats.player_data["Position"])
			var cords = str(PlayerStats.player_data["Position"])
			cords.erase(cords.find("("),1)
			cords.erase(cords.find(")"),1)
			cords.erase(cords.find(","),1)
			position = Vector2(cords.left(cords.find(" ")), cords.right(cords.find(" ")))
			#position = PlayerStats.player_data["Position"] as Vector2
			get_node("ActorGui/Name").text = PlayerStats.pseudo
			pass
		PlayerStats.ActorType.OTHER_PLAYER:
			get_node("HitBox/HitBoxSprite").modulate = Color(0, 1.3, 0, 1)
			get_node("Camera2D").current = false
			pass
		PlayerStats.ActorType.ENEMY:
			get_node("HitBox/HitBoxSprite").modulate = Color(1.3, 0, 0, 1)
			get_node("Camera2D").current = false
			pass
		_:
			print("ERROR: Unknown actor type !!!")
			pass
	get_node("HitBox/AnimationPlayer").play("Turn")
	pass # Replace with function body.


func compute_sprite_direction():
	if motion.x > 0:
		$ActorSprite.scale.x = 1
	elif motion.x < 0:
		$ActorSprite.scale.x = -1
	pass
	
func compute_animation():
	if abs(motion.x) < 0.05 and abs(motion.y) <0.05:
		$AnimationPlayer.play("Idle")
	else:
		$AnimationPlayer.play("Walk")
#	if is_interrupted:
#		# Stunned
#		pass
	pass

func MovePlayer(player_position, animation_vector):
	if actor_type == PlayerStats.ActorType.OTHER_PLAYER:
#	if not player_state == "Attack":
#		#print(position.distance_to(player_position))
#		if position.distance_to(player_position) < 0.01:
#			$AnimationPlayer.play("Idle_"+animation_vector)
#			set_position(player_position)
#		else:
#			$AnimationPlayer.play("Walk_"+animation_vector)
#			set_position(player_position)
		motion = player_position - previous_position
		set_position(player_position)
		previous_position = position
				
func ChangeHealth(value):
	#print("Take damage: " + str(value))
	health = value
	get_node("ActorGui").ChangeHealth(max_health, value)
	
	if health <= 0:
		match actor_type:
			PlayerStats.ActorType.PLAYER:
#				set_physics_process(false)
#				visible = false
#				yield(get_tree().create_timer(1.0), "timeout")
#				queue_free()
				pass
			PlayerStats.ActorType.OTHER_PLAYER:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
			PlayerStats.ActorType.ENEMY:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
			_:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
		
	pass
	
func ChangeHealthFromServer(max_value, value):
	#print("Take damage: " + str(value))
	max_health = max_value
	health = value
	get_node("ActorGui").ChangeHealth(max_value, value)
	
	if health <= 0:
		match actor_type:
			PlayerStats.ActorType.PLAYER:
#				set_physics_process(false)
#				visible = false
#				yield(get_tree().create_timer(1.0), "timeout")
#				queue_free()
				pass
			PlayerStats.ActorType.OTHER_PLAYER:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
			PlayerStats.ActorType.ENEMY:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
			_:
				get_node("HitBox").set_deferred("disabled", true)
				set_physics_process(false)
				visible = false
				yield(get_tree().create_timer(20.0), "timeout")
				queue_free()
				pass
	pass

func ChangeSprite(name):
	get_node("ActorSprite").texture = load("res://Assets/img/player/16bit_" + str(name) + ".png")

func ChangePseudo(name):
	get_node("ActorGui").get_node("Name").text = name

func SetPopupDamage(damage):
	var ftext = FloatingTextClass.instance()
	ftext.value = damage
	ftext.global_position = global_position
	get_parent().add_child(ftext)
	pass








func _on_FirstUpdateTimer_timeout() -> void:
	match actor_type:
		PlayerStats.ActorType.PLAYER:
			get_node("ActorSprite").texture = load("res://Assets/img/player/16bit_" + str(PlayerStats.player_data["Sprite"]) + ".png")
			var cords = str(PlayerStats.player_data["Position"])
			cords.erase(cords.find("("),1)
			cords.erase(cords.find(")"),1)
			cords.erase(cords.find(","),1)
			position = Vector2(cords.left(cords.find(" ")), cords.right(cords.find(" ")))
			#position = PlayerStats.player_data["Position"] as Vector2
			get_node("ActorGui/Name").text = PlayerStats.pseudo
			set_physics_process(true)
			pass
		PlayerStats.ActorType.OTHER_PLAYER:
			pass
		PlayerStats.ActorType.ENEMY:
			pass
		_:
			print("ERROR: Unknown actor type !!!")
			pass
	pass # Replace with function body.









