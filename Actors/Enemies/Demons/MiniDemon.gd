extends "res://Actors/Enemies/Enemy.gd"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	get_node("ActorSprite").texture = load("res://Assets/img/enemies/16bit_mini_demon.png")
	get_node("ActorGui").actor_name.text = "Mini Demon"
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
