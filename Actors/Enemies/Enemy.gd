extends "res://Actors/ActorTemplate.gd"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
#	actor_type = PlayerStats.ActorType.ENEMY
#	get_node("ActorSprite").texture = load("res://Assets/img/enemies/16bit_big_demon.png")
#	get_node("HitBox/HitBoxSprite").modulate = Color(1.3, 0, 0, 1)
#	get_node("Camera2D").current = false
	add_to_group("Enemies")
	previous_position = position
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	compute_sprite_direction()
	compute_animation()
	pass

func MoveEnemy(enemy_position):
	motion = enemy_position - previous_position
	set_position(enemy_position)
	previous_position = position
	
func Health(value):
	if health != value:
		health = value
		#UpdateHPBar()
		if health <= 0:
			#OnDeath()
			pass
