extends "res://Actors/ActorTemplate.gd"

#var spell_test = preload("res://Actors/Spells/Spell.tscn")
var spell_test = preload("res://Actors/Spells/SwingAttack.tscn")
var GhostClass = preload("res://Actors/Players/PlayerGhost.tscn")

var ghost_number = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	add_to_group("Players")
	#actor_type = PlayerStats.ActorType.PLAYER
#	get_node("HitBox/HitBoxSprite").modulate = Color(0, 1.3, 0, 1)
#	get_node("Camera2D").current = true
#	get_node("DodgeParticles").texture = load("res://Assets/img/player/16bit_" + str(PlayerStats.player_data["Sprite"]) + ".png")
#	get_node("ActorGui/Name").text = PlayerStats.pseudo
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float) -> void:
	var current_motion = Vector2.ZERO
	if actor_type == PlayerStats.ActorType.PLAYER:
		var vel = Vector2(0,0)
		vel.x = int(Input.is_action_pressed("p1_right")) - int(Input.is_action_pressed("p1_left"))
		vel.y = int(Input.is_action_pressed("p1_down")) - int(Input.is_action_pressed("p1_up"))
			
		current_motion = vel.normalized() * 120
		if get_node("DodgingTimer").time_left > 0:
			current_motion = dodge(current_motion)
		if Input.is_action_just_pressed("p1_dodge"):
			ghost_number = 0
			get_node("GhostTimer").start()
			get_node("DodgingTimer").start()
			current_motion = dodge(current_motion)
			#$DodgeTrail.emitting = true
		
		#$DodgeTrail.rotation = 0
		#$WorldPattern/DodgeTrail.global_position = global_position
		
		if Input.is_action_just_pressed("p1_spell1"):
			var spell = spell_test.instance()
			spell.global_position = position
			spell.global_rotation = global_rotation
			spell.spell_unique_ID = str(get_tree().get_network_unique_id()) + str(PlayerStats.spell_increment)
			PlayerStats.spell_increment = PlayerStats.spell_increment + 1
			spell.name = spell.spell_unique_ID
			get_parent().get_parent().add_child(spell)
			#spell.show_telegraph(true)
			spell.use_art(0)
		
	#$AnimationPlayer.play("Idle")
	compute_sprite_direction()
	
	if actor_type == PlayerStats.ActorType.PLAYER:
		motion = lerp(motion, current_motion, 0.3)
		move_and_slide(motion)
		
	compute_animation()
	
	if actor_type == PlayerStats.ActorType.PLAYER:
		DefinePlayerState()
	pass

func DefinePlayerState():
	var player_state = {"T": Server.client_clock, "P": get_global_position(), "A": "UP"}
	Server.SendPlayerState(player_state)

func dodge(current_motion):
	#if dodge_pool >= 10:
	current_motion = current_motion * 4
		#dodge_pool = dodge_pool - 10
	
	
	return current_motion


func _on_GhostTimer_timeout() -> void:
	var current_ghost = GhostClass.instance()
	current_ghost.position = position + get_node("ActorSprite").position
	current_ghost.texture = get_node("ActorSprite").texture
	current_ghost.vframes = get_node("ActorSprite").vframes
	current_ghost.hframes = get_node("ActorSprite").hframes
	current_ghost.frame = get_node("ActorSprite").frame
	current_ghost.offset = get_node("ActorSprite").offset
	current_ghost.scale = get_node("ActorSprite").scale
	get_parent().add_child(current_ghost)
	ghost_number = ghost_number + 1
	if ghost_number > 2:
		get_node("GhostTimer").stop()
	pass # Replace with function body.


func _on_DodgingTimer_timeout() -> void:
	pass # Replace with function body.
