extends Area2D

export(int) var spell_ID = 0
var spell_unique_ID = "" #PlayerID + Increment

var original = true
var used = false
var use_date = 0

func _ready() -> void:
	set_physics_process(false)

func destroy_spell():
	set_physics_process(false)
	if original: # Spell created by this player
		visible = false
		yield(get_tree().create_timer(1.0), "timeout")
		queue_free()
	else:
		visible = false
		yield(get_tree().create_timer(1.0), "timeout")
		queue_free()

func _physics_process(delta: float) -> void:
	if original: # Spell created by this player
		# Compute new position
		global_position = get_node("/root/Map/YSort/Player").global_position
#		print("start")
#		print(global_position)
#		print(get_global_mouse_position())
		global_rotation = get_global_mouse_position().angle_to_point(global_position)
		if used:
			Server.SendSpellState(spell_ID, spell_unique_ID, use_date, global_position, global_rotation)
		else:
			pass
	
func SetPositionAndRotation(s_position, s_rotation) -> void:
	if not original: # Spell created by another player
		position = s_position
		global_rotation = s_rotation

func use_art(date) -> void:
	if original: # Spell created by this player
		global_position = get_node("/root/Map/YSort/Player").global_position
		global_rotation = get_global_mouse_position().angle_to_point(global_position)
		use_date = Server.client_clock
		Server.UseSpell(spell_ID, spell_unique_ID, use_date, global_position, global_rotation)
		set_physics_process(true)
		get_node("AnimationPlayer").play("Animate")
		used = true
	else:
		get_node("AnimationPlayer").play("Animate", Server.client_clock - date)
		#get_node("AnimationPlayer").seek(Server.client_clock - date, true)
	art_effect()

func art_effect() -> void:
	#get_node("ShapeParticles").emitting = true
	pass

func show_telegraph(value) -> void:
	if original: # Spell created by this player
		set_physics_process(true)
		get_node("Sprite").visible = value
	pass

func ExecuteCollision():
#	var overlapping_areas = get_overlapping_bodies()
#	for area in overlapping_areas:
#		if area.is_in_group("Enemies"):
#			print("Enemy touched")
#		if area.is_in_group("Players"):
#			print("Player touched")
	pass



