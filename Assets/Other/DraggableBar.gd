extends Panel

var parent = null;
var drag = false;
var offset = Vector2(0, 0);

func _ready():
	parent = get_parent();
	set_process_input(false)

func _input(event : InputEvent):
	#print("draggable input")
	if parent && drag:
		parent.rect_global_position = event.global_position - offset;
		global.loot_panel_position = parent.rect_global_position

func _gui_input(event : InputEvent):
	#print("draggable gui input")
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		drag = event.pressed;
		offset = event.global_position - parent.rect_global_position;

func change_draggable(value):
	set_process_input(value)
