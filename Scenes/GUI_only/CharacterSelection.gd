extends Control

var character_display = preload("res://Scenes/GUI_only/CharacterSelectionDisplay.tscn")
var character_list

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Server.FetchAccountCharacters()
	pass # Replace with function body.

func SetCharacterList(list):
	character_list = list
	
	var current_char_list = get_node("VBoxContainer/CharacterListScroll/CharacterList").get_children()
	for character in current_char_list:
		character.queue_free()
	
	for character in character_list:
		var character_disp = character_display.instance()
		character_disp.SetCharacter(character, character_list[character])
		get_node("VBoxContainer/CharacterListScroll/CharacterList").add_child(character_disp)
	pass

func SelectCharacter(pseudo):
	print("Selected character: " + str(pseudo))
	PlayerStats.pseudo = pseudo
	PlayerStats.player_data = character_list[pseudo]
	Server.LogCharacter(PlayerStats.pseudo)
	get_tree().change_scene("res://Scenes/MainScenes/Map.tscn")
	pass

func DeleteCharacter(pseudo):
	print("Delete character: " + str(pseudo))
	Server.DeleteCharacter(pseudo)
	pass


func _on_CreateCharacterButton_pressed() -> void:
	pass # Replace with function body.


func _on_ExitButton_pressed() -> void:
	EventBus.emit_signal("unlog_server")
	get_tree().change_scene("res://Scenes/GUI_only/LoginScreen.tscn")
	pass # Replace with function body.
