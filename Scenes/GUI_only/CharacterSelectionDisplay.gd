extends Control


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	get_node("HBoxContainer/CenterContainer/AnimationPlayer").play("Move")
	pass # Replace with function body.

func SetCharacter(pseudo, stats):
	get_node("HBoxContainer/CenterContainer/Sprite").texture = load("res://Assets/img/player/16bit_" + str(stats["Sprite"]) + ".png")
	get_node("HBoxContainer/CharName/Pseudo").text = pseudo
	get_node("HBoxContainer/CharName/Level").text = "Level:  " + str(stats["Level"])
	get_node("HBoxContainer/CharStats/Strength").text = "Strength:  " + str(stats["Strength"])
	get_node("HBoxContainer/CharStats/Dexterity").text = "Dexterity:  " + str(stats["Dexterity"])
	get_node("HBoxContainer/CharStats/Constitution").text = "Constitution:  " + str(stats["Constitution"])
	get_node("HBoxContainer/CharStats/Intelligence").text = "Intelligence:  " + str(stats["Intelligence"])
	get_node("HBoxContainer/CharStats/Wisdom").text = "Wisdom:  " + str(stats["Wisdom"])
	pass


func _on_SelectButton_pressed() -> void:
	get_parent().get_parent().get_parent().get_parent().SelectCharacter(get_node("HBoxContainer/CharName/Pseudo").text)
	pass # Replace with function body.


func _on_DeleteButton_pressed() -> void:
	get_parent().get_parent().get_parent().get_parent().DeleteCharacter(get_node("HBoxContainer/CharName/Pseudo").text)
	pass # Replace with function body.
