extends Control

# UI nodes
onready var login_screen = get_node("LoginBackground")
onready var create_account_screen = get_node("CreateAccountBackground")
onready var info_label = get_node("Info")
# Login nodes
onready var username_input = get_node("LoginBackground/LoginTopics/Username")
onready var password_input = get_node("LoginBackground/LoginTopics/Password")
onready var login_button = get_node("LoginBackground/LoginTopics/LoginButtons/LoginButton")
onready var create_account_button = get_node("LoginBackground/LoginTopics/LoginButtons/CreateAccountButton")
onready var exit_button = get_node("LoginBackground/LoginTopics/ExitButton")
# Create account nodes
onready var create_username_input = get_node("CreateAccountBackground/CreateAccountTopics/Username")
onready var create_email_input = get_node("CreateAccountBackground/CreateAccountTopics/Email")
onready var create_userpassword_input = get_node("CreateAccountBackground/CreateAccountTopics/Password")
onready var create_userpassword_repeat_input = get_node("CreateAccountBackground/CreateAccountTopics/RepeatPassword")
onready var confirm_button = get_node("CreateAccountBackground/CreateAccountTopics/CreateAccountButtons/CreateValidationButton")
onready var back_button = get_node("CreateAccountBackground/CreateAccountTopics/CreateAccountButtons/CreateBackButton")

func _ready() -> void:
	EventBus.connect("login_status", self, "_on_LoginStatus_event")
	EventBus.connect("server_status", self, "_on_ServerStatus_event")

func _on_LoginStatus_event(status):
	match(status):
		-1:
			info_label.text = "Fail to connect to authentication server"
			yield(get_tree().create_timer(0.4),"timeout")
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		0:
			info_label.text = "Try to connect to authentication server"
			yield(get_tree().create_timer(0.4),"timeout")
			pass
		1:
			info_label.text = "Try to authenticate"
			yield(get_tree().create_timer(0.4),"timeout")
			pass
		2:
			info_label.text = "Login successfully"
			yield(get_tree().create_timer(0.4),"timeout")
			Server.ConnectToServer()
			pass
		3:
			info_label.text = "Login fail (bad username / password)"
			yield(get_tree().create_timer(0.4),"timeout")
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		4:
			info_label.text = "Account created successfully"
			yield(get_tree().create_timer(0.4),"timeout")
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			_on_CreateBackButton_pressed()
			pass
		5:
			info_label.text = "Fail to create account"
			yield(get_tree().create_timer(0.4),"timeout")
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		6:
			info_label.text = "Fail to create account (username already exist)"
			yield(get_tree().create_timer(0.4),"timeout")
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		_:
			info_label.text = "Unknown status"
			pass
	pass

func _on_ServerStatus_event(status):
	match(status):
		-1:
			info_label.text = "Fail to connect to Game Server"
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		0:
			info_label.text = "Connecting to Game Server"
			pass
		1:
			info_label.text = "Successfully connect to Game Server"
			pass
		2:
			info_label.text = "Game Server disconnected"
			pass
		3:
			info_label.text = "Successfull authentication to Game Server"
			#Server.FetchExistingCharacterName("Djokavex2")
			#Server.FetchExistingCharacterName("CharName2")
			#Server.FetchAccountCharacters()
			get_tree().change_scene("res://Scenes/GUI_only/CharacterSelection.tscn")
			pass
		4:
			info_label.text = "Fail to authenticate to Game Server"
			login_button.disabled = false
			create_account_button.disabled = false
			username_input.editable = true
			password_input.editable = true
			confirm_button.disabled = false
			back_button.disabled = false
			create_username_input.editable = true
			create_email_input.editable = true
			create_userpassword_input.editable = true
			create_userpassword_repeat_input.editable = true
			pass
		_:
			info_label.text = "Unknown status"
			pass
	
	#yield(get_tree().create_timer(0.4),"timeout")
	# Connection status with the Game Server
	# -1 Fail connect to server
	# 0 Connecting to server
	# 1 Server connected
	# 2 Server disconnected
	# 3 Successfull authentication to game server
	# 4 Fail auth to game server
	pass

func _on_LoginButton_pressed() -> void:
	if username_input.text == "" or password_input.text == "":
		print("Invalid user and/or password")
		info_label.text = "Invalid user and/or password"
	else:
		login_button.disabled = true
		create_account_button.disabled = true
		username_input.editable = false
		password_input.editable = false
		var username = username_input.get_text()
		var password = password_input.get_text()
		password_input.text = ""
		print("attempting to login")
		info_label.text = "attempting to login"
		Gateway.ConnectToServer(username, password, "test@test.com", false)


func _on_CreateAccountButton_pressed() -> void:
	login_screen.hide()
	create_account_screen.show()
	info_label.text = ""


func _on_ExitButton_pressed() -> void:
	get_tree().quit()


func _on_CreateValidationButton_pressed() -> void:
	if create_username_input.get_text() == "":
		print("Please provide a valid username")
		info_label.text = "Please provide a valid username"
	elif create_email_input.get_text() == "":
		print("Please provide a valid email")
		info_label.text = "Please provide a valid email"
	elif create_userpassword_input.get_text() == "":
		print("Please provide a valid password")
		info_label.text = "Please provide a valid password"
	elif create_userpassword_repeat_input.get_text() == "":
		print("Please repeat your password")
		info_label.text = "Please repeat your password"
	elif create_userpassword_input.get_text() != create_userpassword_repeat_input.get_text():
		print("Passwords don't match")
		info_label.text = "Passwords don't match"
	elif create_userpassword_input.get_text().length() <= 6:
		print("Password must containt at least 7 characters")
		info_label.text = "Password must containt at least 7 characters"
	else:
		confirm_button.disabled = true
		back_button.disabled = true
		var username = create_username_input.get_text()
		var email = create_email_input.get_text()
		var password = create_userpassword_input.get_text()
		create_username_input.editable = false
		create_email_input.editable = false
		create_userpassword_input.editable = false
		create_userpassword_repeat_input.editable = false
		create_userpassword_input.text = ""
		create_userpassword_repeat_input.text = ""
		Gateway.ConnectToServer(username, password, email, true)


func _on_CreateBackButton_pressed() -> void:
	login_screen.show()
	create_account_screen.hide()
	create_userpassword_input.text = ""
	create_userpassword_repeat_input.text = ""
	info_label.text = ""
