extends Control

onready var username_input = get_node("Username")
onready var password_input = get_node("Password")
onready var login_button = get_node("Login")


func _on_Login_pressed() -> void:
	if username_input.text == "" or password_input.text == "":
		print("Invalid user and/or password")
	else:
		login_button.disabled = true
		var username = username_input.get_text()
		var password = password_input.get_text()
		print("attempting to login")
		Gateway.ConnectToServer(username, password)
	pass # Replace with function body.
