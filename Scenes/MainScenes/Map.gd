extends Node2D

#var player_spawn = preload("res://Scenes/SupportScenes/Player.tscn")
var player_spawn = preload("res://Actors/Players/Player.tscn")
#var enemy_spawn = preload("res://Scenes/SupportScenes/SmallDemon.tscn")
var enemy_spawn = preload("res://Actors/Enemies/Enemy.tscn")
var mini_demon_spawn = preload("res://Actors/Enemies/Demons/MiniDemon.tscn")
var small_demon_spawn = preload("res://Actors/Enemies/Demons/SmallDemon.tscn")
var big_demon_spawn = preload("res://Actors/Enemies/Demons/BigDemon.tscn")
var spell_spawn = preload("res://Actors/Spells/Spell.tscn")

var last_world_state = 0
var world_state_buffer = []
const interpolation_offset = 100

func SpawnNewPlayer(player_id, spawn_position):
	if get_tree().get_network_unique_id() == player_id:
		pass
	else:
		if not get_node("YSort/OtherPlayers").has_node(str(player_id)):
			var new_player = player_spawn.instance()
			new_player.position = spawn_position
			new_player.name = str(player_id)
			new_player.actor_type = PlayerStats.ActorType.OTHER_PLAYER
			new_player.ChangePseudo("Player")
			Server.FetchPlayerDataFromID(player_id)
			get_node("YSort/OtherPlayers").add_child(new_player)
		
func DespawnPlayer(player_id):
	yield(get_tree().create_timer(0.2), "timeout")
	var player_node = get_node_or_null("YSort/OtherPlayers/" + str(player_id))
	if player_node != null:
		player_node.queue_free()

func SpawnNewEnemy(enemy_id, enemy_dict):
	if not get_node("YSort/Enemies").has_node(str(enemy_id)):
		var new_enemy = mini_demon_spawn.instance()
		new_enemy.position = enemy_dict["EnemyLocation"]
		new_enemy.state = enemy_dict["EnemyState"]
		new_enemy.actor_type = PlayerStats.ActorType.ENEMY
		new_enemy.name = str(enemy_id)
		get_node("YSort/Enemies").add_child(new_enemy)

func SpawnNewSpell(spell_id, spell_dict):
	if not has_node(str(spell_id)):
		var new_spell = spell_spawn.instance()
		new_spell.position = spell_dict["SpellLocation"]
		new_spell.rotation_degrees = spell_dict["SpellDirection"]
		#new_spell.actor_type = PlayerStats.ActorType.ENEMY
		new_spell.name = str(spell_id)
		new_spell.spell_unique_ID = str(spell_id)
		new_spell.original = false
		add_child(new_spell)
		new_spell.use_art(spell_dict["StartDate"])
		
func UpdateWorldState(world_state):
	if world_state["T"] > last_world_state:
		last_world_state = world_state["T"]
		world_state_buffer.append(world_state)
		
	
func _physics_process(delta: float) -> void:
	#var render_time = OS.get_system_time_msecs() - interpolation_offset
	var render_time = Server.client_clock - interpolation_offset
	if world_state_buffer.size() > 1:
		while world_state_buffer.size() > 2 and render_time > world_state_buffer[2].T:
			world_state_buffer.remove(0)
		if world_state_buffer.size() > 2:
			var interpolation_factor = float(render_time - world_state_buffer[1]["T"]) / float(world_state_buffer[2]["T"] - world_state_buffer[1]["T"])
			
			# UPDATE PLAYERS STATE
			for player in world_state_buffer[2].keys():
				if str(player) == "T":
					continue
				if str(player) == "Enemies":
					continue
				if str(player) == "Spells":
					continue
				if player == get_tree().get_network_unique_id():
					continue
				if not world_state_buffer[1].has(player):
					continue
				
				if get_node("YSort/OtherPlayers").has_node(str(player)):
					# Player already exist -> Update him
					var new_position = lerp(world_state_buffer[1][player]["P"], world_state_buffer[2][player]["P"], interpolation_factor)
					var animation_vector = world_state_buffer[2][player]["A"]
					get_node("YSort/OtherPlayers/" + str(player)).MovePlayer(new_position, animation_vector)
				else:
					# Player does not exist -> Spawn him
					print("spawning player")
					SpawnNewPlayer(player, world_state_buffer[2][player]["P"])
			
			# UPDATE ENEMIES STATE
			for enemy in world_state_buffer[2]["Enemies"].keys():
				if not world_state_buffer[1]["Enemies"].has(enemy):
					continue
				if get_node("YSort/Enemies").has_node(str(enemy)):
					# Enemy already exist -> Update him
					var new_position = lerp(world_state_buffer[1]["Enemies"][enemy]["EnemyLocation"], world_state_buffer[2]["Enemies"][enemy]["EnemyLocation"], interpolation_factor)
					get_node("YSort/Enemies/" + str(enemy)).MoveEnemy(new_position)
					get_node("YSort/Enemies/" + str(enemy)).ChangeHealthFromServer(world_state_buffer[2]["Enemies"][enemy]["EnemyMaxHealth"], world_state_buffer[2]["Enemies"][enemy]["EnemyHealth"])
					#print(str(world_state_buffer[1]["Enemies"][enemy]["EnemyHealth"]) + " " + str(world_state_buffer[2]["Enemies"][enemy]["EnemyHealth"]))
				else:
					# Enemy does not exist -> Spawn him
					SpawnNewEnemy(enemy, world_state_buffer[2]["Enemies"][enemy])
			
			# UPDATE SPELLS STATE
			for spell in world_state_buffer[2]["Spells"].keys():
				if not world_state_buffer[1]["Spells"].has(spell):
					continue
				if has_node(str(spell)):
					# Enemy already exist -> Update him
					if world_state_buffer[2]["Spells"][spell]["SpellSender"] == get_tree().get_network_unique_id():
						# Own spell
						#print("My own spell")
						pass
					else:
						var new_position = lerp(world_state_buffer[1]["Spells"][spell]["SpellLocation"], world_state_buffer[2]["Spells"][spell]["SpellLocation"], interpolation_factor)
						var prev_rotation = world_state_buffer[1]["Spells"][spell]["SpellDirection"]
						var next_rotation = world_state_buffer[2]["Spells"][spell]["SpellDirection"]
						if next_rotation > (prev_rotation + PI):
							next_rotation = next_rotation - 2*PI
						if next_rotation < (prev_rotation - PI):
							next_rotation = next_rotation + 2*PI
						var new_rotation = lerp(prev_rotation, next_rotation, interpolation_factor)
						get_node("" + str(spell)).SetPositionAndRotation(new_position, new_rotation)
						#get_node("YSort/Enemies/" + str(spell)).ChangeHealthFromServer(world_state_buffer[2]["Spells"][spell]["EnemyMaxHealth"], world_state_buffer[2]["Spells"][spell]["EnemyHealth"])
						#print(str(world_state_buffer[1]["Enemies"][enemy]["EnemyHealth"]) + " " + str(world_state_buffer[2]["Enemies"][enemy]["EnemyHealth"]))
				else:
					# Enemy does not exist -> Spawn him
					SpawnNewSpell(spell, world_state_buffer[2]["Spells"][spell])
					
			# TODO
		elif render_time > world_state_buffer[1].T: 
			# No future world state
			var extrapolation_factor = float(render_time - world_state_buffer[0]["T"]) / float(world_state_buffer[1]["T"] - world_state_buffer[0]["T"]) - 1.00
			
			# UPDATE PLAYERS STATE
			for player in world_state_buffer[1].keys():
				if str(player) == "T":
					continue
				if str(player) == "Enemies":
					continue
				if str(player) == "Spells":
					continue
				if player == get_tree().get_network_unique_id():
					continue
				if not world_state_buffer[0].has(player):
					continue
					
				if get_node("YSort/OtherPlayers").has_node(str(player)):
					var position_delta = (world_state_buffer[1][player]["P"] - world_state_buffer[0][player]["P"])
					var new_position = world_state_buffer[1][player]["P"] + (position_delta * extrapolation_factor)
					var animation_vector = world_state_buffer[1][player]["A"]
					get_node("YSort/OtherPlayers/" + str(player)).MovePlayer(new_position, animation_vector)
			
			# UPDATE ENEMIES STATE
			for enemy in world_state_buffer[1]["Enemies"].keys():
				if not world_state_buffer[0]["Enemies"].has(enemy):
					continue
				if get_node("YSort/Enemies").has_node(str(enemy)):
					# Enemy already exist -> Update him
					var position_delta = (world_state_buffer[1]["Enemies"][enemy]["EnemyLocation"] - world_state_buffer[0]["Enemies"][enemy]["EnemyLocation"])
					var new_position = world_state_buffer[1]["Enemies"][enemy]["EnemyLocation"] + (position_delta * extrapolation_factor)
					get_node("YSort/Enemies/" + str(enemy)).MoveEnemy(new_position)
					get_node("YSort/Enemies/" + str(enemy)).ChangeHealthFromServer(world_state_buffer[1]["Enemies"][enemy]["EnemyMaxHealth"], world_state_buffer[1]["Enemies"][enemy]["EnemyHealth"])
					#print(str(world_state_buffer[1]["Enemies"][enemy]["EnemyHealth"]) + " " + str(world_state_buffer[2]["Enemies"][enemy]["EnemyHealth"]))
				else:
					# Enemy does not exist -> Spawn him
					SpawnNewEnemy(enemy, world_state_buffer[1]["Enemies"][enemy])
					
			# UPDATE SPELLS STATE
			for spell in world_state_buffer[1]["Spells"].keys():
				if not world_state_buffer[0]["Spells"].has(spell):
					continue
				if has_node(str(spell)):
					# Enemy already exist -> Update him
					if world_state_buffer[1]["Spells"][spell]["SpellSender"] == get_tree().get_network_unique_id():
						# Own spell
						#print("My own spell")
						pass
					else:
						var position_delta = (world_state_buffer[1]["Spells"][spell]["SpellLocation"] - world_state_buffer[0]["Spells"][spell]["SpellLocation"])
						var rotation_delta = (world_state_buffer[1]["Spells"][spell]["SpellDirection"] - world_state_buffer[0]["Spells"][spell]["SpellDirection"])
						var new_position = world_state_buffer[1]["Spells"][spell]["SpellLocation"] + (position_delta * extrapolation_factor)
						var new_rotation = world_state_buffer[1]["Spells"][spell]["SpellDirection"] + (rotation_delta * extrapolation_factor)
						get_node("" + str(spell)).SetPositionAndRotation(new_position, new_rotation)
						#get_node("YSort/Enemies/" + str(spell)).ChangeHealthFromServer(world_state_buffer[2]["Spells"][spell]["EnemyMaxHealth"], world_state_buffer[2]["Spells"][spell]["EnemyHealth"])
						#print(str(world_state_buffer[1]["Enemies"][enemy]["EnemyHealth"]) + " " + str(world_state_buffer[2]["Enemies"][enemy]["EnemyHealth"]))
				else:
					# Enemy does not exist -> Spawn him
					SpawnNewSpell(spell, world_state_buffer[1]["Spells"][spell])
			# TODO

func UpdateWorldState_OLD(world_state):
	if world_state["T"] > last_world_state:
		last_world_state = world_state["T"]
		world_state.erase("T")
		world_state.erase(get_tree().get_network_unique_id())
		for player in world_state.keys():
			if get_node("YSort/OtherPlayers").has_node(str(player)):
				get_node("YSort/OtherPlayers/" + str(player)).MovePlayer(world_state[player]["P"])
			else:
				print("spawning player")
				SpawnNewPlayer(player, world_state[player]["P"])
