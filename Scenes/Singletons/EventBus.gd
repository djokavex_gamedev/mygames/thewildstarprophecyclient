extends Node

# Connection status with the Gateway
# -1 fail to connect
# 0 connecting
# 1 connected -> authenticating
# 2 login success
# 3 login fail (bad username / pass)
# 4 Account created successfully
# 5 Fail create account
# 6 Fail create account (username already exist)
signal login_status(status)

# Connection status with the Game Server
# -1 Fail connect to server
# 0 Connecting to server
# 1 Server connected
# 2 Server disconnected
# 3 Successfull authentication to game server
# 4 Fail auth to game server
signal server_status(status)

signal unlog_server()
