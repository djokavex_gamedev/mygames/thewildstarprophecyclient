extends Node

enum ActorType {
	PLAYER,
	OTHER_PLAYER,
	ENEMY
}

var base_strength = 0
var base_dexterity = 0
var base_constitution = 0
var base_intelligence = 0
var base_wisdom = 0

var player_data
var pseudo = ""

var spell_increment = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Server.FetchPlayerStats()
	pass # Replace with function body.

func LoadPlayerStats(stats):
	base_strength = stats.Strength
	base_constitution = stats.Constitution
	base_dexterity = stats.Dexterity
	base_intelligence = stats.Intelligence
	base_wisdom = stats.Wisdom
	pass
