extends Control

# UI nodes
onready var login_screen = get_node("NinePatchRect/LoginScreen")
onready var create_account_screen = get_node("NinePatchRect/CreateAccount")
# Login nodes
onready var username_input = get_node("NinePatchRect/LoginScreen/Username")
onready var password_input = get_node("NinePatchRect/LoginScreen/Password")
onready var login_button = get_node("NinePatchRect/LoginScreen/LoginButton")
onready var create_account_button = get_node("NinePatchRect/LoginScreen/CreateAccountButton")
# Create account nodes
onready var create_username_input = get_node("NinePatchRect/CreateAccount/Username")
onready var create_userpassword_input = get_node("NinePatchRect/CreateAccount/Password")
onready var create_userpassword_repeat_input = get_node("NinePatchRect/CreateAccount/RepeatPassword")
onready var confirm_button = get_node("NinePatchRect/CreateAccount/ConfirmButton")
onready var back_button = get_node("NinePatchRect/CreateAccount/BackButton")

func _on_LoginButton_pressed() -> void:
	if username_input.text == "" or password_input.text == "":
		print("Invalid user and/or password")
	else:
		login_button.disabled = true
		create_account_button.disabled = true
		var username = username_input.get_text()
		var password = password_input.get_text()
		print("attempting to login")
		Gateway.ConnectToServer(username, password, "test@test.com", false)
	pass # Replace with function body.


func _on_CreateAccountButton_pressed() -> void:
	login_screen.hide()
	create_account_screen.show()
	pass # Replace with function body.


func _on_ConfirmButton_pressed() -> void:
	if create_username_input.get_text() == "":
		print("Please provide a valid username")
	elif create_userpassword_input.get_text() == "":
		print("Please provide a valid password")
	elif create_userpassword_repeat_input.get_text() == "":
		print("Please repeat your password")
	elif create_userpassword_input.get_text() != create_userpassword_repeat_input.get_text():
		print("Passwords don't match")
	elif create_userpassword_input.get_text().length() <= 6:
		print("Password must containt at least 7 characters")
	else:
		confirm_button.disabled = true
		back_button.disabled = true
		var username = create_username_input.get_text()
		var password = create_userpassword_input.get_text()
		Gateway.ConnectToServer(username, password, "test@test.com", true)
	pass # Replace with function body.


func _on_BackButton_pressed() -> void:
	login_screen.show()
	create_account_screen.hide()
	pass # Replace with function body.
