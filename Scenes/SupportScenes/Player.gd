extends KinematicBody2D

var ShotClass = preload("res://Scenes/SupportScenes/Shot.tscn")

export var current_player = false

var motion = Vector2.ZERO
var previous_direction = "RIGHT"

var player_state

var attack_dict = {}

func _ready() -> void:
	set_physics_process(false)
	if not current_player:
		set_physics_process(true)
		$Camera2D.current = false
		$Sprite.modulate = Color(0.9, 0, 0)
		pass

func _physics_process(delta) -> void:
	if current_player:
		MovementLoop(delta)
		DefinePlayerState()
	else:
		if not attack_dict == {}:
			Attack()

func MovementLoop(delta):
	var vel = Vector2(0,0)
	vel.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	vel.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	
	if vel.x == 0.0 and vel.y == 0:
		$AnimationPlayer.play("Idle_"+previous_direction)
	else:
		if vel.y > 0:
			previous_direction = "DOWN"
		elif vel.y < 0:
			previous_direction = "UP"
		if vel.x > 0:
			previous_direction = "RIGHT"
		elif vel.x < 0:
			previous_direction = "LEFT"
		
		$AnimationPlayer.play("Walk_"+previous_direction)

	var current_motion = vel.normalized() * 200
	
	motion = lerp(motion, current_motion, 0.2)
	motion = move_and_slide(motion)
	
	if Input.is_action_just_pressed("ui_accept"):
		Attack()


func Attack():
	if current_player:
		
		
		var shot = ShotClass.instance()
		if previous_direction == "RIGHT":
			shot.impulse_rotation = 0
		elif previous_direction == "LEFT":
			shot.impulse_rotation = 3.14
		elif previous_direction == "UP":
			shot.impulse_rotation = -1.57
		elif previous_direction == "DOWN":
			shot.impulse_rotation = 1.57
		
		shot.position = position
		#shot.original = true
		
		Server.SendAttack(position, previous_direction, shot.position, shot.impulse_rotation)
		get_parent().add_child(shot)
	else:
		for attack in attack_dict.keys():
			if attack <= Server.client_clock:
				player_state = "Attack"
				#cast animation
				# ??? set_position(attack_dict[attack]["Position"])
				previous_direction = attack_dict[attack]["AnimationVector"]
				
				print("Spawn shot")
				var shot = ShotClass.instance()
				if previous_direction == "RIGHT":
					shot.impulse_rotation = 0
				elif previous_direction == "LEFT":
					shot.impulse_rotation = 3.14
				elif previous_direction == "UP":
					shot.impulse_rotation = -1.57
				elif previous_direction == "DOWN":
					shot.impulse_rotation = 1.57
				
				shot.position = attack_dict[attack]["Position"]
				#shot.original = false
				
				attack_dict.erase(attack)
				#yield(get_tree().create_timer(0.2), "timeout")
				get_parent().add_child(shot)
				#yield(get_tree().create_timer(0.2), "timeout")
				
				player_state = "Idle"
				

func DefinePlayerState():
	player_state = {"T": Server.client_clock, "P": get_global_position(), "A": previous_direction}
	Server.SendPlayerState(player_state)


func MovePlayer(player_position, animation_vector):
	if not current_player:
		if not player_state == "Attack":
			#print(position.distance_to(player_position))
			if position.distance_to(player_position) < 0.01:
				$AnimationPlayer.play("Idle_"+animation_vector)
				set_position(player_position)
			else:
				$AnimationPlayer.play("Walk_"+animation_vector)
				set_position(player_position)

