extends Control

onready var strenght = get_node("NinePatchRect/VBoxContainer/Strenght/StatValue")
onready var vitality = get_node("NinePatchRect/VBoxContainer/Vitality/StatValue")
onready var dexterity = get_node("NinePatchRect/VBoxContainer/Dexterity/StatValue")
onready var intelligence = get_node("NinePatchRect/VBoxContainer/Intelligence/StatValue")
onready var wisdom = get_node("NinePatchRect/VBoxContainer/Wisdom/StatValue")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Server.FetchPlayerStats()
	pass # Replace with function body.

func LoadPlayerStats(stats):
	strenght.set_text(str(stats.Strenght))
	vitality.set_text(str(stats.Vitality))
	dexterity.set_text(str(stats.Dexterity))
	intelligence.set_text(str(stats.Intelligence))
	wisdom.set_text(str(stats.Wisdom))
	pass
