extends RigidBody2D

#var original = true

var projectile_speed = 400
var impulse_rotation
#var damage = 10

func _ready():
	#Server.FetchSkillDamage("Shot", get_instance_id())
	apply_impulse(Vector2(), Vector2(projectile_speed, 0).rotated(impulse_rotation))
	SelfDestruct()

func SelfDestruct():
	yield(get_tree().create_timer(3.0), "timeout")
	queue_free()

#func SetDamage(s_damage):
#	damage = s_damage

func _on_Shot_body_entered(body):
	get_node("CollisionShape2D").set_deferred("disabled", true)
#	if body.is_in_group("Enemies") and original == true:
#		body.ChangeHealth(-damage)
	self.hide()
	pass # Replace with function body.
