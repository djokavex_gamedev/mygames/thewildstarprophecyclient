extends KinematicBody2D

var max_health = 500
var health = 500
var state = "Idle"

func _ready():
	add_to_group("Enemies")
	$Bar/HealthBar.max_value = max_health
	$Bar/HealthBar.value = health
	#TODO Add already dead stuff
	if state == "Dead":
		get_node("CollisionShape2D").set_deferred("disabled", true)
		$Bar/HealthBar.hide()
		z_index = -1
		$DeadTimer.start()
		set_physics_process(false)

func _physics_process(delta: float) -> void:
	if state == "Dead":
		OnDeath()
	pass

#func ChangeHealth(value):
#	print("Take damage: " + str(value))
#	Server.NPCHit(int(get_name()), value)
##	health = health + value
##	UpdateHPBar()
##	if health <= 0:
##		OnDeath()
#	pass

func MoveEnemy(enemy_position):
	set_position(enemy_position)
	
func Health(value):
	if health != value:
		health = value
		UpdateHPBar()
		if health <= 0:
			OnDeath()

func OnDeath():
	#get_node("CollisionShape2D").set_deferred("disabled", true)
	$Bar/HealthBar.hide()
	z_index = -1
	$DeadTimer.start()
	set_physics_process(false)
	yield(get_tree().create_timer(0.2),"timeout")
	get_node("CollisionShape2D").set_deferred("disabled", true)
	
func UpdateHPBar():
	$Bar/HealthBar/Tween.interpolate_property($Bar/HealthBar, "value", $Bar/HealthBar.value, health, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Bar/HealthBar/Tween.start()
	pass


func _on_DeadTimer_timeout() -> void:
	queue_free()
	pass # Replace with function body.
